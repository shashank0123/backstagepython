from django.apps import AppConfig


class CrossappmodulesConfig(AppConfig):
    name = 'crossappmodules'
