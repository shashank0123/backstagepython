from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import get_object_or_404


def Homepage(request):
    return render(request, 'index.html')
