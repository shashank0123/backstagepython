from django.conf.urls import url, include
from django.urls import path
from . import views
from rest_framework import permissions

urlpatterns = [
    url(r'^$', views.Projectslist.as_view()),
    path('create/', views.CreateProject.as_view()),
    path('module/add', views.AddModule.as_view()),
    path('process/', views.ProcessProject.as_view())
]
