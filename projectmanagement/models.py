from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class ProjectType(models.Model):
    project_type = models.CharField(max_length=80)

    def __str__(self):
        return self.project_type


class ProjectLanguage(models.Model):
    language = models.CharField(max_length=80)

    def __str__(self):
        return self.language


class ProjectTemplate(models.Model):
    template_name = models.CharField(max_length=80)
    template_location = models.CharField(
        default=None, blank=True, null=True, max_length=80)
    template_language = models.ForeignKey(
        ProjectLanguage, on_delete=models.CASCADE)

    def __str__(self):
        return self.language


class ProjectTheme(models.Model):
    project_language_id = models.ForeignKey(
        ProjectLanguage, on_delete=models.CASCADE)
    theme_name = models.CharField(max_length=80)
    theme_location = models.CharField(
        default=None, blank=True, null=True, max_length=80)


class Projects(models.Model):
    STATUS_TYPE = (
        ('Initiated', 'Initiated'),
        ('Designing', 'Designing'),
        ('WireFrame', 'WireFrame'),
        ('Setup', 'Setup'),
        ('Development', 'Development'),
        ('Prototype', 'Prototype'),
        ('Testing', 'Testing'),
        ('Delivery', 'Delivery'),
    )
    STATUS_OPTION = (
        ('A', 'Active'),
        ('D', 'Deactive'),
    )
    project_name = models.CharField(max_length=200)
    project_start = models.DateField(default=None, blank=True, null=True)
    project_end = models.DateField(default=None, blank=True, null=True)
    project_location = models.CharField(
        default=None, blank=True, null=True, max_length=500)
    project_type_id = models.ForeignKey(
        ProjectType, on_delete=models.CASCADE, default=1)
    project_url = models.CharField(
        default=None, blank=True, null=True, max_length=500)
    logo = models.ImageField(upload_to='images/', null=True)
    project_git = models.CharField(
        default=None, blank=True, null=True, max_length=500)
    project_amount = models.IntegerField(
        default=None, blank=True, null=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(default=None, blank=True,
                              null=True, max_length=500, choices=STATUS_OPTION)
    current_status = models.CharField(
        default=None, blank=True, null=True, max_length=500, choices=STATUS_TYPE)
    project_language_id = models.ForeignKey(
        ProjectLanguage, null=True, on_delete=models.CASCADE)
    project_setting = models.CharField(
        default=None, blank=True, null=True, max_length=500)

    def __str__(self):
        return self.project_name


class ProjectPayment(models.Model):
    project_id = models.ForeignKey(Projects, on_delete=models.CASCADE)
    amount = models.IntegerField(default=None, blank=True, null=True)
    payment_date = models.DateTimeField(default=None, blank=True, null=True)
    mode = models.CharField(default=None, blank=True, null=True, max_length=40)

    def __str__(self):
        return self.project_id+" - "+self.amount


class ProjectTask(models.Model):
    project_id = models.ForeignKey(Projects, on_delete=models.CASCADE)
    task_name = models.CharField(max_length=200)
    assigned_to = models.ForeignKey(User, on_delete=models.CASCADE)
    start_date = models.DateTimeField(default=None, blank=True, null=True)
    finished_on = models.DateTimeField(default=None, blank=True, null=True)

    def __str__(self):
        return self.project_id+" - "+self.task_name


class ProjectModule(models.Model):
    PROCESS_OPTION = (
        ('0', 'Yes'),
        ('1', 'No'),
    )
    STATUS_OPTION = (
        ('A', 'Active'),
        ('D', 'Deactive'),
    )
    project_id = models.ForeignKey(Projects, on_delete=models.CASCADE)
    module_name = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    primary_key = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    theme_id = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    feature_for = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    controller_name = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    view_folder_name = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    options_to_add = models.CharField(
        default=None, blank=True, null=True, max_length=200, choices=PROCESS_OPTION)
    status = models.CharField(default=None, blank=True,
                              null=True, max_length=200, choices=STATUS_OPTION)
    is_template = models.CharField(
        default=None, blank=True, null=True, max_length=200, choices=PROCESS_OPTION)
    is_processed = models.CharField(
        default=None, blank=True, null=True, max_length=200, choices=PROCESS_OPTION)

    def __str__(self):
        return self.project_id+" - "+self.module_name


class ProjectModuleField(models.Model):
    module_id = models.ForeignKey(ProjectModule, on_delete=models.CASCADE)
    field_name = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    field_type = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    size = models.CharField(
        default='100', blank=True, null=True, max_length=200)


class ProjectTemplateTask(models.Model):
    template_id = models.ForeignKey(ProjectTemplate, on_delete=models.CASCADE)
    function_name = models.CharField(max_length=200)
    function_priority = models.CharField(
        default=None, blank=True, null=True, max_length=200)
    commands = models.CharField(
        default=None, blank=True, null=True, max_length=200)

    def __str__(self):
        return self.template_id+" - "+self.function_name
