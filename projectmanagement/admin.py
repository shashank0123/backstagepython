from django.contrib import admin
from .models import ProjectType
from .models import ProjectLanguage
from .models import ProjectTemplate
from .models import Projects
from .models import ProjectPayment
from .models import ProjectTask
from .models import ProjectModule
from .models import ProjectTemplateTask


# Register your models here.
admin.site.register(ProjectType)
admin.site.register(ProjectLanguage)
admin.site.register(ProjectTemplate)
admin.site.register(Projects)
admin.site.register(ProjectPayment)
admin.site.register(ProjectTask)
admin.site.register(ProjectModule)
admin.site.register(ProjectTemplateTask)
