from rest_framework import serializers
from .models import ProjectType
from .models import ProjectLanguage
from .models import ProjectTemplate
from .models import Projects
from .models import ProjectPayment
from .models import ProjectTask
from .models import ProjectModule
from .models import ProjectTemplateTask


class ProjectTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectType
        fields = '__all__'


class ProjectLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectLanguage
        fields = '__all__'


class ProjectPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectPayment
        fields = '__all__'


class ProjectTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectTemplate
        fields = '__all__'


class ProjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = '__all__'


class ProjectTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectTask
        fields = '__all__'


class ProjectModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectModule
        fields = '__all__'


class ProjectTemplateTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectTemplateTask
        fields = '__all__'
