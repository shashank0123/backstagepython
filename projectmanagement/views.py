from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import ProjectType
from .models import ProjectLanguage
from .models import ProjectTemplate
from .models import Projects
from .models import ProjectPayment
from .models import ProjectTask
from .models import ProjectModule
from .models import ProjectTemplateTask
from .serializers import ProjectTypeSerializer
from .serializers import ProjectLanguageSerializer
from .serializers import ProjectTemplateSerializer
from .serializers import ProjectsSerializer
from .serializers import ProjectPaymentSerializer
from .serializers import ProjectTaskSerializer
from .serializers import ProjectModuleSerializer
from .serializers import ProjectTemplateTaskSerializer


class ProjectTypelist(APIView):
    def get(self, request):
        tvshows1 = ProjectType.objects.all()
        serializer = ProjectTypeSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass


class ProjectLanguagelist(APIView):
    def get(self, request):
        tvshows1 = ProjectLanguage.objects.all()
        serializer = ProjectLanguageSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass


class ProjectTemplatelist(APIView):
    def get(self, request):
        tvshows1 = ProjectTemplate.objects.all()
        serializer = ProjectTemplateSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass


class Projectslist(APIView):
    def get(self, request):
        tvshows1 = Projects.objects.all()
        serializer = ProjectsSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass


class CreateProject(APIView):
    serializer_class = ProjectsSerializer

    def post(self, request):
        serializer = ProjectsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddModule(APIView):
    serializer_class = ProjectModuleSerializer

    def post(self, request):
        serializer = ProjectModuleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProcessProject(APIView):
    serializer_class = ProjectsSerializer

    def post(self, request):
        serializer = ProjectsSerializer(data=request.data)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProjectPaymentlist(APIView):
    def get(self, request):
        tvshows1 = ProjectPayment.objects.all()
        serializer = ProjectPaymentSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass


class ProjectTasklist(APIView):
    def get(self, request):
        tvshows1 = ProjectTask.objects.all()
        serializer = ProjectTaskSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass


class ProjectModulelist(APIView):
    def get(self, request):
        tvshows1 = ProjectModule.objects.all()
        serializer = ProjectModuleSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass


class ProjectTemplateTasklist(APIView):
    def get(self, request):
        tvshows1 = ProjectTemplateTask.objects.all()
        serializer = ProjectTemplateTaskSerializer(tvshows1, many=True)
        return Response(serializer.data)
        pass

    def post(self):
        pass
