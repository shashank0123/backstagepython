from django.apps import AppConfig


class TvmanagementConfig(AppConfig):
    name = 'tvmanagement'
