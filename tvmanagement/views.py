from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import tvshows
from .serializers import tvshowserializer

class Tvshowlist(APIView):
    def get(self, request):
        tvshows1=tvshows.objects.all()
        serializer=tvshowserializer(tvshows1, many=True)
        return Response(serializer.data)
        pass
    def post(self):
        pass