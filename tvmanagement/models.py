from django.db import models

class tvshows(models.Model):
    SHOW_TYPE = (
        ('movie', 'Movie'),
        ('Tvshow', 'TV-Show'),
        ('shorts', 'Short Video'),
    )
    STATUS_TYPE = (
        ('A', 'Active'),
        ('D', 'Deactive'),
    )
    show_name=models.CharField(default=None,blank=True, null=True,max_length=400)
    short_desc=models.CharField(default=None,blank=True, null=True,max_length=4000)
    description=models.CharField(default=None,blank=True, null=True,max_length=4000)
    show_type=models.CharField(default=None,blank=True, null=True,max_length=200,choices=SHOW_TYPE)
    poster=models.CharField(default=None,blank=True, null=True,max_length=4000)
    backdrop_path=models.CharField(default=None,blank=True, null=True,max_length=4000)
    tmdb_id=models.IntegerField(default=None,blank=True, null=True)
    jw_id=models.IntegerField(default=None,blank=True, null=True)
    imdb_id=models.IntegerField(default=None,blank=True, null=True)
    other_id=models.IntegerField(default=None,blank=True, null=True)
    release_date=models.DateField(default=None,blank=True, null=True)
    number_of_episode=models.IntegerField(default=None,blank=True, null=True)
    number_of_seasons=models.IntegerField(default=None,blank=True, null=True)
    last_episode=models.DateField(default=None,blank=True, null=True)
    current_status=models.CharField(default=None,blank=True, null=True,max_length=20)
    status=models.CharField(default=None,blank=True, null=True,max_length=200,choices=STATUS_TYPE)

    def __str__(self):
        return self.show_name