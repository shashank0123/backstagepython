from rest_framework import serializers
from .models import tvshows

class tvshowserializer(serializers.ModelSerializer):
    
    class Meta:
        model= tvshows
        # fields=('show_name', 'description', 'release_date', 'show_type', 'poster', 'backdrop_path', )
        fields='__all__'